class AdminController < ApplicationController
  def top
    @users = User.all
    @items  = Item.all
    @item = Item.new
  end
  def add
    @item = Item.new(item_name:params[:item_name], item_price:params[:item_price], item_stock:params[:item_stock], item_img:'1.jpg')
    @item.save
    redirect_to admin_top_path
  end
end

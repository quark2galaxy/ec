class UserController < ApplicationController
  def top
    @user = User.new
  end
  def new
    @user = User.new(name:params[:name], mail:params[:mail], password:params[:password])
    @user.save
    redirect_to user_top_path
  end
  def login
    @user = User.find_by(mail:params[:mail], password:params[:password])
    @items = Item.all
    @item = Item.new
    session[:user_id] = @user.id
  end
  def login_top
    @user = User.find_by_id(session[:user_id])
    @items = Item.all
    session[:user_id] = @user.id
  end
  def logout
    session[:user_id] = nil;
    redirect_to user_top_path
  end
  def cart_add
    @cart = Cart.new(user_id:session[:user_id], item_id:params[:item_id], number:params[:number])
    @cart.save
    redirect_to user_login_top_path
  end
  def cart_show
    @carts = Cart.joins(:item).select('carts.id as cart_id, items.id as item_id, items.item_img as item_img, items.item_name as item_name, items.item_price as item_price, items.item_stock as stock, carts.number as number').where('carts.user_id=?', session[:user_id])
    #@carts = Cart.where('user_id = ?', session[:user_id])
  end
  def cart_destroy
    @cart = Cart.find_by_id(params[:cart_id])
    @cart.destroy
    redirect_to user_login_top_path
  end
  def cart_update
    @cart = Cart.find_by_id(params[:cart_id])
    @cart.update(number:params[:number])
    redirect_to user_login_top_path
  end
  def buy
    @carts = Cart.joins(:item).select('carts.id as cart_id, items.id as item_id, items.item_stock as stock, carts.number as number').where('carts.user_id=?', session[:user_id])
    @carts.each do |cart|
      @item = Item.find_by_id(cart.item_id)
      @item.update(item_stock:cart.stock-cart.number)
    end
    @carts = Cart.all.where('user_id = ?', session[:user_id])
    @carts.each do |cart|
      cart.destroy
    end
    redirect_to user_login_top_path
  end
end

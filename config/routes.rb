Rails.application.routes.draw do
  root 'index#top'
  get 'admin_top', to:'admin#top', as:'admin_top'
  get 'user_top', to:'user#top', as:'user_top'
  post 'user_new', to:'user#new', as:'user_new'
  post 'user_login', to:'user#login', as:'user_login'
  get 'user_logout', to:'user#logout', as:'user_logout'
  post 'item_add', to:'admin#add', as:'item_add'
  post 'cart_add', to:'user#cart_add', as:'cart_add'
  get 'user_login_top', to:'user#login_top', as:'user_login_top'
  get 'cart_show', to: 'user#cart_show', as:'cart_show'
  #post 'admin_item_add', to:'admin#add', as:'admin_item_top'
  post 'cart_destroy', to:'user#cart_destroy', as:'cart_destroy'
  post 'cart_update', to:'user#cart_update', as:'cart_update'
  post 'buy', to:'user#buy', as:'buy'
  #get 'index/top'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
